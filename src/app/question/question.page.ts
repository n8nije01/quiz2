import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuizService } from '../quiz.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {

  private questionCounter: number;
  private optionCounter: number;
  private startTime: Date;
  private endTime: Date;
  private duration: number;
  private feedback: string;
  private points: number;

  constructor(public router: Router, private quizService: QuizService) { }

  ngOnInit() {
    this.points = 0;
    this.quizService.addQuestions();
    this.quizService.initialize();
    this.feedback = "";
  }

  public getPoints() {
    return this.points;
  }

  private checkOption(option: number) {
    this.quizService.setOptionSelected();
    if (this.quizService.areAllOptionsUsed() ) {
      this.quizService.setQuestion();
    }
    else {
      if (this.quizService.isCorrectOption(option)) {
        this.points++;
        this.feedback =
        this.quizService.getCorrectOptionOfActiveQuestion() +
        ' is correct! Go on...';
        this.continue();
      }
      else {
        this.feedback = 'The answer is InCorrect';
        this.continue();
      }
    }
  }
  public setQuestion() {
    this.optionCounter = 0;
    this.questionCounter++;
  }

  public initialize() {
    this.questionCounter = 0;
    this.startTime = new Date();
    this.setQuestion();
  }

  public getDuration(): number {
    this.endTime = new Date();
    this.duration = this.endTime.getTime() - this.startTime.getTime();
    return this.duration;
  }

  private continue() {
    if (this.quizService.isFinished()) {
      this.points = this.getPoints();
      this.duration = this.quizService.getDuration();
      this.router.navigateByUrl('result/' + this.duration + '/' + this.points);
    }
    else {
      this.quizService.setQuestion();
    }
  }



}
