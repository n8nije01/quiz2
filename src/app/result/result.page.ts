import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuizService } from '../quiz.service';


@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  private correctness: number;
  private duration: number;
  private durationSeconds: number;
  public points: number;
  public message: string;
  private statusImage: string;
  private readonly imagePath: string = "../assets/imgs/";
  private questionCounter: number;



  constructor(public router: Router, private quizService: QuizService,
    public activatedRoute: ActivatedRoute) { }



  ngOnInit() {
    this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
    this.durationSeconds = Math.round((this.duration) / 1000);
    this.points = Number(this.activatedRoute.snapshot.paramMap.get('points'));
    this.correctness = (this.points * 20);
    this.questionCounter = this.quizService.questionCounter;
  }

  private printResult() {
    if (this.points === 5 && this.durationSeconds < 20) {
      this.message = "Excellent. You knew everything";
      this.imagePrint();
    } else if (this.points === 4 && this.durationSeconds < 30) {
      this.message = "Very good, you can still do better";
      this.imagePrint();
    } else if (this.points === 3 && this.durationSeconds < 40) {
      this.message = "Okay, you need more practise";
      this.imagePrint();
    } else if (this.points === 2 && this.durationSeconds < 50) {
      this.message = "You need to practise";
      this.imagePrint();
    } else if (this.points === 1 && this.durationSeconds < 60) {
      this.message = "Not good at all";
      this.imagePrint();
    } else {
      this.message = "Oh No! how that happened!";
      this.imagePrint();
    }
    return this.message;
  }

  private imagePrint() {
    return this.statusImage = this.imagePath + this.points + ".png";
  }

  toAnswers() {
    this.quizService.initialize();
    this.router.navigateByUrl('answers');
  }








}
