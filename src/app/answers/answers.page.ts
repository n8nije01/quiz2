import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuizService } from '../quiz.service';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.page.html',
  styleUrls: ['./answers.page.scss'],
})
export class AnswersPage implements OnInit {

  

  constructor(public router: Router, private quizService: QuizService,
    public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  toHome() {
    this.quizService.initialize();
    this.router.navigateByUrl('home');
  }

}
