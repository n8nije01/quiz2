import { Injectable, OnInit } from '@angular/core';
import { Question } from '../question';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  private questions: Question[] = []; //kysymykset
  private activeQuestion: Question; //aktiivinen kysymys
  private isCorrect: boolean; //kertoo mikä oikein
  private isSelected: boolean; //on valittu
  public questionCounter: number; //laskuri, milloin kysymykset loppuu
  private optionCounter: number; //moneen kertaan saa vastata kysymykseen
  private startTime: Date; //aloitus aika
  private endTime: Date; //lopetus aika
  private duration: number; //kesto
  public message: string;

  constructor() { }

  public addQuestions() {
    this.questions = [{
      quest: 'What is latin name of a fox?',
      options: [
        'Canis lupus familiaris',
        'Canis lupus',
        'Vulpes vulpes',
        'Lynx lynx'],
      correctOption: 2
    },
    {
      quest: 'What is latin name of a black bear?',
      options: [
        'Ursus americanus',
        'Ursus arctos',
        'Equus caballus caballus',
        'Testudines'],
      correctOption: 0
    },
    {
      quest: 'What is latin name of a sea-urchin?',
      options: [
        'Carcharodon carcharias',
        'Erinaceus europaeus',
        'Holothuroidea',
        'Echinoidea'],
      correctOption: 3
    },
    {
      quest: 'What is latin name of a hawk?',
      options: [
        'Cyanistes caeruleus',
        'Accipitridae',
        'Turdus merula',
        'Lepidoptera'],
      correctOption: 1
    },
    {
      quest: 'What is latin name of a snake?',
      options: [
        'Felis catus',
        'Gastropoda',
        'Serpentes',
        'Vipera berus'],
      correctOption: 2
    }
    ];
  }

  public setQuestion() {
    this.optionCounter = 0; //nollaa valitut vastaukset
    this.isCorrect = false; //nollaa ovatko vastaukset oikein
    this.isSelected = false; //onko valittu
    this.activeQuestion = this.questions[this.questionCounter]; //nollaa taulukon jossa kysymykset ja vastaukset
    this.questionCounter++; //kasvatetaan laskurin arvoa
  }

  public initialize() { //nollaa sovelluksen
    this.questionCounter = 0;
    this.startTime = new Date();
    this.setQuestion();
  }

  public getQuestions(): Question[] { //funktio joka palauttaa kaikki kysymykset
    return this.questions;
  }

  public getActiveQuestion(): Question { //funktio joka palauttaa akttivisen kysymyksen
    return this.activeQuestion; //.quote palauttaa myös
  }

  //tuo vastauksen näkyviin
  public getQuestionOfActiveQuestion(): string { //aktiivisen kysymyksen lainauksen
    return this.activeQuestion.quest;
  }

  public getOptionsOfActiveQuestion(): string[] { //palauttaa string taulukon jossa on: aktiivisten kysymysten optiot
    return this.activeQuestion.options;
  }

  public getIndexOfActiveQuestion(): number { //mieti nimeämistä
    return this.questionCounter; //aktiivisen kysymyksen indeksi, monesko menossa
  }

  public getNumberOfQuestions(): number { //kuinka monta kysymystä
    return this.questions.length;
  }

  public getNumberOfOptionsOfActiveQuestion(): number {
    return this.activeQuestion.options.length; //montako optioita on
  }

  public getIndexOfOptionCounter(): number {
    return this.optionCounter;
  }

  public getCorrectOptionOfActiveQuestion(): string { //palauttaa oikea optio
    return this.activeQuestion.options[this.activeQuestion.correctOption];
  }

  public setOptionSelected() {
    this.isSelected = true;
  }

  public isOptionSelected(): boolean {
    return this.isSelected;
  }

  public areAllOptionsUsed(): boolean {
    this.optionCounter++;
    return (this.optionCounter > this.activeQuestion.options.length) ? true : false;
  }

  public isCorrectOption(option: number): boolean {
    this.isCorrect = (option === this.activeQuestion.correctOption) ? true : false;
    return this.isCorrect;
  }

  public isAnswerCorrect(): boolean {
    return this.isCorrect;
  }

  public isFinished(): boolean {
    return (this.questionCounter === this.questions.length) ? true : false;
  }

  public getDuration(): number {
    this.endTime = new Date();
    this.duration = this.endTime.getTime() - this.startTime.getTime();
    return this.duration;
  }



}
