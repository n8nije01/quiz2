export class Question {
    quest: string;
    options: string[];
    correctOption: number;
}
